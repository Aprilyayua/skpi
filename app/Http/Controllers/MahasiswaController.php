<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index()
    {

    	$mahasiswa = DB::table('mahasiswa')->get();
 
    	return view('index',['mahasiswa' => $mahasiswa]);
 
    }

    public function dashboard()
    {
        return view('read.v_dashboard');
    }
}
