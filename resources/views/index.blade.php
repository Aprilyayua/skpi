<!DOCTYPE html>
<html>
<head>
	<title>CRUD MAHASISWA</title>
</head>
<body>

	<h3>Data Mahasiswa</h3>

	<a href="/mahasiswa/tambah"> + Tambah Mahasiswa Baru</a>
	
	<br/>
	<br/>

	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Jurusan</th>
			<th>Prodi</th>
			<th>Keterangan</th>
		</tr>
		@foreach($mahasiswa as $m)
		<tr>
			<td>{{ $m->nama }}</td>
			<td>{{ $m->jurusan }}</td>
			<td>{{ $m->prodi }}</td>
			<td>
				<a href="/mahasiswa/edit/{{ $m->id }}">Edit</a>
				|
				<a href="/mahasiswa/hapus/{{ $m->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>


</body>
</html>